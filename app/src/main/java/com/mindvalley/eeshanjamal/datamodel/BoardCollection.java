/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.datamodel;

import java.util.ArrayList;
import java.util.List;

public class BoardCollection {

    private List<AImageCard> data;

    public BoardCollection(){
        data = new ArrayList<>();
    }

    public BoardCollection(List<AImageCard> data){
        setData(data);

    }

    public void setData(List<AImageCard> data){
        this.data = data;
    }

    public List<AImageCard> getData() {
        return data;
    }


}
