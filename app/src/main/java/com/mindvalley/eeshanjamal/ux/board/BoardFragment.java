/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.ux.board;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mindvalley.eeshanjamal.R;

import com.mindvalley.eeshanjamal.context.MVContextManager;
import com.mindvalley.eeshanjamal.datamodel.AImageCard;
import com.mindvalley.eeshanjamal.datamodel.BoardCollection;

import java.util.ArrayList;
import java.util.List;

public class BoardFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private RecyclerView mList;
    private SwipeRefreshLayout mRefreshView;
    private Context context;
    private OnListFragmentInteractionListener mListener;

    public BoardFragment() {
    }

    public static BoardFragment newInstance(int columnCount) {
        BoardFragment fragment = new BoardFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view       = inflater.inflate(R.layout.fragment_board_list, container, false);
        context         = view.getContext();

        mList           = (RecyclerView) view.findViewById(R.id.list);
        mRefreshView    = (SwipeRefreshLayout) view.findViewById(R.id.refresh_view);

        mRefreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestListData();
            }
        });

        setupAdapter(new ArrayList<AImageCard>());
        requestListData();

        return view;
    }

    private void showLoader(){
        mRefreshView.post(new Runnable() {
            @Override
            public void run() {
                mRefreshView.setRefreshing(true);
            }
        });
    }

    private void hideLoader(){
        mRefreshView.post(new Runnable() {
            @Override
            public void run() {
                mRefreshView.setRefreshing(false);
            }
        });
    }

    private void requestListData(){

        showLoader();

        MVContextManager.getInstance().loadBoardCollection(context, "http://pastebin.com/raw/wgkJgazE", null, new MVContextManager.BoardCollectionCallback() {

            @Override
            public void onSuccess(final BoardCollection collection) {
                if (getActivity()!=null){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setupAdapter(collection.getData());
                            hideLoader();
                        }
                    });
                }


            }

            @Override
            public void onFailure(final Exception error) {

                if (getActivity()!=null ){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(getView(), error.getMessage(), Snackbar.LENGTH_LONG).show();
                            hideLoader();
                        }
                    });

                }


            }

        });
    }

    private void setupAdapter(List<AImageCard> data){

        if (mColumnCount <= 1) {
            mList.setLayoutManager(new LinearLayoutManager(context));
        } else {

            mList.setLayoutManager(new StaggeredGridLayoutManager(mColumnCount, LinearLayoutManager.VERTICAL));
        }

        mList.setAdapter(new BoardRecyclerViewAdapter(context, mList.getLayoutManager(), data, mListener));
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return mList.getLayoutManager();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(AImageCard item);
    }

}
