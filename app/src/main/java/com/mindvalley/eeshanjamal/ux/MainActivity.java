/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.ux;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mindvalley.eeshanjamal.R;
import com.mindvalley.eeshanjamal.ux.board.BoardFragment;
import com.mindvalley.eeshanjamal.datamodel.AImageCard;

public class MainActivity extends AppCompatActivity implements BoardFragment.OnListFragmentInteractionListener{

    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private BoardFragment boardFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        toolbar             = (Toolbar) findViewById(R.id.toolbar);
        coordinatorLayout   = (CoordinatorLayout) findViewById(R.id.root_content);

        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setImageResource(R.mipmap.airplane);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boardFragment.getLayoutManager().scrollToPosition(0);
            }
        });

        boardFragment = BoardFragment.newInstance(2);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, boardFragment).commit();
    }

    @Override
    public void onListFragmentInteraction(AImageCard item) {

    }
}
