/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.ux.board;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindvalley.eeshanjamal.R;
import com.mindvalley.eeshanjamal.ux.board.BoardFragment.OnListFragmentInteractionListener;
import com.mindvalley.eeshanjamal.context.MVContextManager;
import com.mindvalley.eeshanjamal.datamodel.AImageCard;
import com.mindvalley.eeshanjamal.lib.webservices.ImageWebService;

import java.util.List;

public class BoardRecyclerViewAdapter extends RecyclerView.Adapter<BoardRecyclerViewAdapter.ViewHolder>{

    private final List<AImageCard> mValues;
    private final Context mContext;
    private final OnListFragmentInteractionListener mListener;
    private final RecyclerView.LayoutManager mLayoutManager;

    public BoardRecyclerViewAdapter(Context context, RecyclerView.LayoutManager layoutManager, List<AImageCard> items, OnListFragmentInteractionListener listener) {
        mContext        = context;
        mLayoutManager  = layoutManager;
        mValues         = items;
        mListener       = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.fragment_board_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.item = mValues.get(position);

        holder.image.setBackgroundColor(Color.parseColor(holder.item.color));
        holder.likesCount.setText(String.valueOf(holder.item.likes));
        holder.likeIcon.setSupportBackgroundTintList(ColorStateList.valueOf(holder.item.liked_by_user?Color.RED:Color.LTGRAY));
        holder.name.setText(holder.item.user.name);

        MVContextManager.getInstance().loadImage(mContext, holder.item.urls.small, new ImageWebService.ImageWebServiceCallback() {

            @Override
            public void onSuccess(String url, final Bitmap bitmap) {

                if (holder.item.urls.small.equals(url)){

                    if (mContext instanceof Activity){
                        ((Activity) mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int layoutWidth  = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, bitmap.getWidth(), mContext.getResources().getDisplayMetrics());
                                int layoutHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, bitmap.getHeight(), mContext.getResources().getDisplayMetrics());

                                holder.image.setLayoutParams(new LinearLayout.LayoutParams(layoutWidth, layoutHeight));
                                holder.image.setImageBitmap(bitmap);
                            }
                        });
                    }

                }

            }

            @Override
            public void onFailure(Exception e) {

            }

        });

        MVContextManager.getInstance().loadImage(mContext, holder.item.user.profile_image.small, new ImageWebService.ImageWebServiceCallback() {

            @Override
            public void onSuccess(String url, final Bitmap bitmap) {

                if (holder.item.user.profile_image.small.equals(url)){

                    if (mContext instanceof Activity){
                        ((Activity) mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                int layoutWidth  = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, bitmap.getWidth(), mContext.getResources().getDisplayMetrics());
                                int layoutHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, bitmap.getHeight(), mContext.getResources().getDisplayMetrics());

                                holder.userThumbnail.setLayoutParams(new LinearLayout.LayoutParams(layoutWidth, layoutHeight));
                                holder.userThumbnail.setImageBitmap(bitmap);
                            }
                        });
                    }

                }

            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.item);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public AImageCard item;
        public final View mView;
        public ImageView image;
        public AppCompatImageView likeIcon;
        public TextView likesCount;
        public TextView name;
        public ImageView userThumbnail;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            image           = (ImageView) view.findViewById(R.id.image);
            likeIcon        = (AppCompatImageView) view.findViewById(R.id.like_image);
            userThumbnail   = (ImageView) view.findViewById(R.id.user_thumbnail);
            name            = (TextView) view.findViewById(R.id.name);
            likesCount      = (TextView) view.findViewById(R.id.likes_count);
        }

    }
}
