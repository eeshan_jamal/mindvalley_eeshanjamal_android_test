/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.context;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mindvalley.eeshanjamal.datamodel.AImageCard;
import com.mindvalley.eeshanjamal.datamodel.BoardCollection;
import com.mindvalley.eeshanjamal.lib.MindValley;
import com.mindvalley.eeshanjamal.lib.cache.ImageCache;
import com.mindvalley.eeshanjamal.lib.cache.JSONCache;
import com.mindvalley.eeshanjamal.lib.webservices.ImageWebService;
import com.mindvalley.eeshanjamal.lib.webservices.JSONWebService;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.Future;

public class MVContextManager {

    private static final int THREAD_POOL_SIZE       =   2;
    private static final int JSON_CACHE_SIZE        =   25;
    private static final int IMAGE_CACHE_SIZE       =   50;

    private static MVContextManager instance;

    /**
     * interface defined to notify the caller about the board collection data.
     */

    public interface BoardCollectionCallback {
        void onSuccess(BoardCollection boardCollection);
        void onFailure(Exception error);
    }

    /**
     * This method return the singleton instance of the context for using it throughout the app.
     */

    public static MVContextManager getInstance(){
        if (instance == null){
            instance = new MVContextManager();
        }
        return instance;
    }

    private MindValley library;

    /**
     * Default constructor creates an instance of the context with
     * library initialization for networking processes.
     */

    private MVContextManager(){
        library = new MindValley(THREAD_POOL_SIZE, new ImageCache(IMAGE_CACHE_SIZE), new JSONCache(JSON_CACHE_SIZE));
    }

    /**
     * This method request the MindValley library to return the requested data information through Cache/Network.
     *
     * @param context The current context making this request.
     *
     * @param url String url having the network api path.
     *
     * @param headers Headers need to bind with the request.
     *
     * @param callback Callback instance for handling the resposne.
     *
     ** @return The Future's object representing pending completion of the task. It can be used
     * to cancel the submitted task. The Future's get method will return null upon successful completion.
     *
     * <br></br>
     * <br></br>
     *
     * <b>Note:</> The returned future task might be null in-case rejection exception occurs.
     *
     */

    public Future loadBoardCollection(Context context, String url, Map<String, String> headers, final BoardCollectionCallback callback){

        return library.loadJsonData(context, url, headers, new JSONWebService.JSONObjectWebServiceCallback() {

            @Override
            public void onSuccess(JsonElement response) {
                AImageCard[] cards = new Gson().fromJson(response, AImageCard[].class);
                BoardCollection boardCollection = new BoardCollection(Arrays.asList(cards));
                if (callback!=null)
                    callback.onSuccess(boardCollection);
            }

            @Override
            public void onFailure(Exception error) {
                if (callback!=null)
                    callback.onFailure(error);
            }

        });

    }

    /**
     * This method request the MindValley library to return the requested image through Cache/Network.
     *
     * @param context The current context making this request.
     *
     * @param url String url having the network api path.
     *
     *
     * @param callback Callback instance for handling the resposne.
     *
     ** @return The Future's object representing pending completion of the task. It can be used
     * to cancel the submitted task. The Future's get method will return null upon successful completion.
     *
     * <br></br>
     * <br></br>
     *
     * <b>Note:</> The returned future task might be null in-case rejection exception occurs.
     *
     */

    public Future loadImage(Context context, String url, ImageWebService.ImageWebServiceCallback callback){
        return library.loadImage(context, url, callback);
    }

}
