/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.lib;

import android.content.Context;

import com.mindvalley.eeshanjamal.lib.cache.ImageCache;
import com.mindvalley.eeshanjamal.lib.cache.JSONCache;
import com.mindvalley.eeshanjamal.lib.webservices.ImageWebService;
import com.mindvalley.eeshanjamal.lib.webservices.JSONWebService;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public class MindValley {

    private static final int DEFAULT_THREAD_POOL_SIZE   =   1;

    private ExecutorService executorService;
    private ImageCache imageCache;
    private JSONCache jsonCache;
    private int threadPoolSize;

    /**
     * This constructor initialize the service with the
     * default thread pool size of 1 and ignore the caching of images and json.
     */

    public MindValley(){
        initExecutorService(DEFAULT_THREAD_POOL_SIZE);
    }

    /**
     * This constructor initialize the service with the
     * provided arguments.
     *
     * @param threadPoolSize Make sure pool size should be > 0 otherwise
     *                       it will be initialized as default thread pool size.
     *
     * @param imageCache Cache which will be use by the service to cache images. Passing
     *                   null will result in ignoring caching of images by the service.
     *
     * @param jsonCache Cache which will be use by the service to cache data. Passing
     *                  null will result in ignoring caching of data by the service.
     *
     */

    public MindValley(int threadPoolSize, ImageCache imageCache, JSONCache jsonCache){
        initExecutorService((threadPoolSize>0)?threadPoolSize:DEFAULT_THREAD_POOL_SIZE);
        this.imageCache = imageCache;
        this.jsonCache  = jsonCache;
    }

    private void initExecutorService(int threadPoolSize){
        executorService = Executors.newFixedThreadPool(threadPoolSize);
        this.threadPoolSize = threadPoolSize;
    }

    /**
     * This method returns the image cache currently in use by the service.
     *
     * <br></br>
     * <br></br>
     *
     * <b>Note:</> It might return null in-case no cache is setup for this service.
     */

    public ImageCache getImageCache() {
        return imageCache;
    }

    /**
     * This method returns the Json cache currently in use by the service.
     *
     * <br></br>
     * <br></br>
     *
     * <b>Note:</b> It might return null in-case no cache is setup for this service.
     */

    public JSONCache getJsonCache() {
        return jsonCache;
    }

    /**
     * This method load the image from Cache/Server.
     *
     * @param context Context instance requesting this service.
     *
     * @param url String containing the url need to be call.
     *
     * @param callback Callback instance notify about the request success/failure.
     *
     * @return The Future's object representing pending completion of the task. It can be used
     * to cancel the submitted task. The Future's get method will return null upon successful completion.
     *
     * <br></br>
     * <br></br>
     *
     * <b>Note:</> The returned future task might be null in-case rejection exception occurs.
     *
     */

    public Future loadImage(Context context, String url, ImageWebService.ImageWebServiceCallback callback){

        try {
            return executorService.submit(new ImageWebService(context, url, imageCache, callback));
        }
        catch (RejectedExecutionException e){
            e.printStackTrace();
            if (callback!=null)
                callback.onFailure(e);
        }

        return null;
    }

    /**
     * This method load the Json data from Cache/Server.
     *
     * @param context Context instance requesting this service.
     *
     * @param url String containing the url need to be call.
     *
     * @param headers Headers which needs to be send with the request.
     *
     * @param callback Callback instance notify about the request success/failure.
     *
     * @return The Future's object representing pending completion of the task. It can be used
     * to cancel the submitted task. The Future's get method will return null upon successful completion.
     *
     * <br></br>
     * <br></br>
     *
     * <b>Note:</> The returned future task might be null in-case rejection exception occurs.
     *
     */

    public Future loadJsonData(Context context, String url, Map<String, String> headers, final JSONWebService.JSONObjectWebServiceCallback callback) {
        try {
            return executorService.submit(new JSONWebService(context, url, headers, jsonCache, callback));
        }
        catch (RejectedExecutionException e){
            e.printStackTrace();
            if (callback!=null)
                callback.onFailure(e);
        }

        return null;
    }

    /**
     * This method post the Json data to server but doesn't cache the response as it's
     * not usable for this kind of request.
     *
     * @param context Context instance requesting this service.
     *
     * @param url String containing the url need to be call.
     *
     * @param headers Headers which needs to be send with the request.
     *
     * @param body String containing the post information data.
     *
     * @param callback Callback instance notify about the request success/failure.
     *
     * @return The Future's object representing pending completion of the task. It can be used
     * to cancel the submitted task. The Future's get method will return null upon successful completion.
     *
     * <br></br>
     * <br></br>
     *
     * <b>Note:</> The returned future task might be null in-case rejection exception occurs.
     *
     */

    public Future postJsonData(Context context, String url, Map<String, String> headers, String body, final JSONWebService.JSONObjectWebServiceCallback callback) {
        try {
            return executorService.submit(new JSONWebService(context, url, headers, body, null, callback));
        }
        catch (RejectedExecutionException e){
            e.printStackTrace();
            if (callback!=null)
                callback.onFailure(e);
        }

        return null;
    }

    /**
     * This method sends the Images/Strings information to the server
     * using form method but doesn't cache the response as it's
     * not usable for this kind of request.
     *
     * @param context Context instance requesting this service.
     *
     * @param url String containing the url need to be call.
     *
     * @param headers Headers which needs to be send with the request.
     *
     * @param requestParams Map having the form key/value(String, File).
     *
     * @param callback Callback instance notify about the request success/failure.
     *
     * @return The Future's object representing pending completion of the task. It can be used
     * to cancel the submitted task. The Future's get method will return null upon successful completion.
     *
     * <br></br>
     * <br></br>
     *
     * <b>Note:</> The returned future task might be null in-case rejection exception occurs.
     *
     */

    public Future postFormData(Context context, String url, Map<String, String> headers, Map<String, Object> requestParams, final JSONWebService.JSONObjectWebServiceCallback callback) {
        try {
            return executorService.submit(new JSONWebService(context, url, headers, requestParams, null, callback));
        }
        catch (RejectedExecutionException e){
            e.printStackTrace();
            if (callback!=null)
                callback.onFailure(e);
        }

        return null;
    }

    /**
     *
     * This method attempts to cancel execution of this task. This attempt will fail if the task has already completed,
     * has already been cancelled, or could not be cancelled for some other reason.
     * If successful, and this task has not started when cancel is called, this task should never run.
     *
     * @param future The Future's object referencing the previously submitted task.
     *
     * @return false if the task could not be cancelled, typically because it has already completed normally; true otherwise.
     */

    public boolean cancel(Future future){
        return future.cancel(true);
    }

    /**
     * This method attempts to stop all actively executing tasks, halts the processing of waiting tasks.
     * It will also re-create a new executor for submitting future task through this service.
     *
     */

    public void cancel(){

        if(!executorService.isShutdown()){
            executorService.shutdownNow();
        }

        initExecutorService(threadPoolSize);

    }

    /**
     * This method will shutdown the current service instance forever after which
     * no future task should be submitted through this service.
     *
     * <br></br>
     * <br></br>
     *
     * <b>Note:</> This method should be called only when this service instance not needed anymore.
     */

    public void shutdown(){

        if(executorService.isShutdown()){
            //No need to do anything
        }
        else{
            executorService.shutdownNow();
            //Wait for two seconds to allow the executor to finish it's running tasks.
            try {
                executorService.awaitTermination(2, TimeUnit.SECONDS);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
