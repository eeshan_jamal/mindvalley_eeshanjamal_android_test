/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.lib.cache;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;

public class ImageCache {

    private static final String TAG = "ash_image_cache";
    private LruCache<String, Bitmap> cache;

    public ImageCache(int maxSize){
    	cache = new LruCache<>(maxSize);
    }

    public LruCache<String, Bitmap> getCache() {
        return cache;
    }

    public Bitmap get(String id){

    	Bitmap b = null;

        try{
            b = cache.get(id);
        }
        catch(NullPointerException ex){
            ex.printStackTrace();
            Log.e(TAG, ex.getMessage()+" at get(...) of ImageCache");
        }

        return b;
    }

    public void put(String id, Bitmap bitmap){
        try{
            cache.put(id, bitmap);
        }
        catch(Throwable th){
            th.printStackTrace();
            Log.e(TAG, th.getMessage()+" at put(...) of ImageCache");
        }
    }
    
    public void clear(String id) {
        try{
            cache.remove(id);
        }catch(NullPointerException ex){
            ex.printStackTrace();
            Log.e(TAG, ex.getMessage()+" at Clear(...) of ImageCache");
        }
    }
    
    public void clear() {
        cache.evictAll();
    }
    
}