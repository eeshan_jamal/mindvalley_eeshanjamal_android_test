/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.lib.webservices;

import android.content.Context;
import android.util.Log;

import com.mindvalley.eeshanjamal.BuildConfig;
import com.mindvalley.eeshanjamal.R;
import com.mindvalley.eeshanjamal.lib.utility.Utils;
import com.mindvalley.eeshanjamal.lib.webservices.support.JResponseError;
import com.mindvalley.eeshanjamal.lib.webservices.support.MultipartUtility;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public abstract class WebService {
    

	protected static final int REQ_TIMEOUT	= 1000*30;//30 Seconds
	public static final String TAG			= "ash_web_service";
	
	protected ServerReuqestMethod requestMethod;
	protected Map<String, String> headers;
	protected String requestUrl;
	protected String requestBody;
	protected Map<String, Object> requestParams;

	protected Context ctx;

	private boolean interrupted;

    public enum ServerReuqestMethod{
    	OPEN, GET, POST, MULTIPART
    }
    
    public WebService(Context ctx){
    	this.ctx = ctx;
    }

	/**
	 * This method can be used by sub class to setup request params.
	 *
	 * @param requestUrl Url having the server api path.
	 *
	 * @param requestMethod Server request method.
	 *
	 * @param headers Headers information which is going to be bind with the connection.
	 *
	 * @param requestBody Information which is going to be bind with the POST connection.
	 *
	 * @param requestParams Information which is going to be bind with MULTI-PART connection.
	 *
	 */

	protected void setupReqParams(ServerReuqestMethod requestMethod, String requestUrl, Map<String, String> headers,
							   String requestBody, Map<String, Object> requestParams){

		this.requestMethod 	= requestMethod;
		this.requestUrl		= requestUrl;
		this.headers		= headers;
		this.requestBody	= requestBody;
		this.requestParams	= requestParams;

	}

	/**
	 * This method can only be used by subclass to call API and they don't
	 * have to provider any argument because it's already defined in the sub
	 * class constructor.
	 */

	protected void connect(){
		connect(requestUrl, requestMethod, headers, requestBody, requestParams);
	}

	/**
	 * This method is useful for using it directly through this class instead of
	 * defining a subclass structure for it. It can be used for making OPEN, GET & POST calls.
	 *
	 * @param apiUrl Url having the server api path.
	 *
	 * @param method Server request method.
	 *
	 * @param headers Headers information which is going to be bind with the connection.
	 *
	 * @param body Information which is going to be bind with the connection.
	 */

    public void connect(String apiUrl, ServerReuqestMethod method, Map<String, String> headers, String body){
    	connect(apiUrl, method, headers, body, null);
    }

	/**
	 * This method is useful for using it directly through this class instead of
	 * defining a subclass structure for it. It can be used for making all method type calls.
	 *
	 * @param apiUrl Url having the server api path.
	 *
	 * @param method Server request method.
	 *
	 * @param headers Headers information which is going to be bind with the connection.
	 *
	 * @param body Information which is going to be bind with the POST connection.
	 *
	 * @param requestParams Information which is going to be bind with MULTI-PART connection.
	 *
	 */

    public void connect(String apiUrl, ServerReuqestMethod method, Map<String, String> headers, String body, Map<String, Object> requestParams){

    	HttpURLConnection conn = null;
    	boolean isErrorStream = false;
    	interrupted = false;
    	
        try{

			if (!Utils.isNetworkConnected(ctx)){
				throw JResponseError.getNetConnectionError(ctx);
			}

            URL url = new URL(apiUrl);
            
            if(BuildConfig.DEBUG){
	            Log.d(TAG, "Request url is  ====>>> "+url);
	            Log.d(TAG, "Request Json is ====>>> "+requestParams);
	            Log.d(TAG, "Request Headers ====>>> "+((headers!=null)?headers.toString():"NULL"));
            }    
            
            conn = getConnection(url, method, headers);
            conn.connect();
            
            if(onConnection(conn)){
                
            	if(method == ServerReuqestMethod.POST && body!=null ){
            		
            		OutputStreamWriter writer= new OutputStreamWriter(conn.getOutputStream());
                    writer.write(body);
                    writer.close();
                    
            	}
            	else if(method == ServerReuqestMethod.MULTIPART){
            		
            		MultipartUtility multipart = new MultipartUtility(conn, "UTF-8");
    				
            		HashMap<String, File> imagesData = new HashMap<String, File>();
            		
    				//======Send Form Fields
    				
    				if(requestParams!=null){
    					for(Iterator<String> iter = requestParams.keySet().iterator();iter.hasNext();) {
    					    
    						String key = iter.next();
    					    
    					    if(requestParams.get(key) instanceof File){
    					    	imagesData.put(key, (File) requestParams.get(key));
    					    }
    					    else{
        					    multipart.addFormField(key, String.valueOf(requestParams.get(key)));
    					    }
    					    
    					}
    				}
    				
    				//====Send Form Images
    				
    				multipart.addFormField("action","saveObject");
    				
    				for(Iterator<String> iter = imagesData.keySet().iterator();iter.hasNext();) {
						
        				String key = iter.next();
        				multipart.addFilePart(key, imagesData.get(key));
        				
        				Log.d(TAG, "Image Field Added =====> "+imagesData.get(key));
					}
    				
    				multipart.completeInput();
            		
            	}
            	
            	/*
            	 *  I put the try catch block here just because it's not necessary that
            	 *  error stream is not null. It gets all the data after an attempt 
            	 *  to get data from server.   
            	 */
            	
            	InputStream stream = null;
            	
            	try{
            		stream = conn.getInputStream();
            	}
            	catch(IOException e){
            		e.printStackTrace();
            		Log.e(TAG,e.getMessage()+" At connect(...) of WebService");
            		
            		stream = conn.getErrorStream();
            		isErrorStream = true;
            	}
            	
            	if(stream==null){
            		Log.i(TAG,"Null Stream!!!");
            		onError(new JResponseError(ctx.getResources().getString(R.string.null_response_msg)));
            	}
            	else if(isErrorStream){
            		Log.i(TAG,"Error Stream!!!");
    				onErrorStream(stream);
    			}
				else if(interrupted){
					onError(new JResponseError(ctx.getResources().getString(R.string.interrupted_response_msg)));
				}
				else{
                
            		if(onStreamReceived(stream)){
                		/*Nothing need to be done here as the response is already processed by sub-class.*/
                	}
            		else{
	                	
                    	String responseStr = getString(stream);

                    	Log.d(TAG,"Response String ===>>> "+responseStr);
                    	
                    	if(responseStr==null || responseStr.equals("null")){
                    		onError(new JResponseError(ctx.getResources().getString(R.string.null_response_msg)));
                    	}
                    	else{

							Log.d(TAG,"On Response Recieved");
							onResponseReceived(responseStr);
                			
                    	}
            		}
	            	
            	}     
                
            }
            else{
            	Log.d(TAG, "Error In Connection!!!");
            	onError(new JResponseError("Error in Connection!!!"));
            }    
            
            conn.disconnect();

        }
        catch(MalformedURLException e){
        	e.printStackTrace();
        	Log.e(TAG, e.getMessage()+" Occurs At Request Api Operation MalformedURLException Catched");
        	Log.e(TAG, "Connection Error Stream ==== >>> "+getErrorStream(conn));
            onError(e);
        }
        catch(IOException e){
        	e.printStackTrace();
        	Log.e(TAG, e.getMessage()+" Occurs At Request Api Operation IOException Catched");
        	Log.e(TAG, "Connection Error Stream ==== >>> "+getErrorStream(conn));
            onError(e);
        }
        catch(OutOfMemoryError e){
        	e.printStackTrace();
        	Log.e(TAG, e.getMessage()+" Occurs At Request Api Operation OutOfMemoryError Catched");
        	Log.e(TAG, "Connection Error Stream ==== >>> "+getErrorStream(conn));
        	Runtime.getRuntime().gc();
            onError(new JResponseError(e.getMessage()));
        }
        catch(NullPointerException e){
        	e.printStackTrace();
        	Log.e(TAG, e.getMessage()+" Occurs At Request Api Operation NullPointerException Catched");
        	Log.e(TAG, "Connection Error Stream ==== >>> "+getErrorStream(conn));
            onError(e);
        }
        catch (JResponseError e) {	
        	e.printStackTrace();
        	Log.e(TAG, e.getMessage()+" Occurs At Request Api Operation JSONException Catched");
        	Log.e(TAG, "Connection Error Stream ==== >>> "+getErrorStream(conn));
			onError(e);
		}
        catch(Exception e){
        	e.printStackTrace();
        	Log.e(TAG, e.getMessage()+" Occurs At Request Api Operation Exception Catched");
        	Log.e(TAG, "Connection Error Stream ==== >>> "+getErrorStream(conn));
            onError(e);
        }
    	
    }

	/**
	 * This method is useful in getting connection error stream incase
	 * any error occurred during connection establishment.
	 *
	 * @param connection In which exception has been raised.
	 *
	 * @return Returns the InputStream If available otherwise returns null.
	 *
	 */

    private InputStream getErrorStream(HttpURLConnection connection){
    	
    	InputStream stream = null;
    	
    	try{
    		stream = connection.getErrorStream();
    	}
    	catch(Exception e){
    		e.getStackTrace();
    		Log.e(TAG, e.getMessage()+" At getErrorStream(...) of WebService");
    	}
    	
    	return stream;
    	
    }

	/**
	 * This method create a connection with the provided information as an arguments.
	 *
	 * @param url URL instance having the url information.
	 *
	 * @param method Defines the connection type.
	 *
	 * @param headers Headers information going to be bind with the connection.
	 *
	 * @return The newly created instance of HttpURLConnection.
	 *
	 * @throws IOException If any error occurred during connection preparation.
	 *
	 */

    private HttpURLConnection getConnection(URL url, ServerReuqestMethod method, Map<String, String> headers) throws IOException{
    	
    	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    	
    	connection.setConnectTimeout(REQ_TIMEOUT);
    	addHeaders(connection, headers);
    	
        switch (method) {
        
			case GET:
				connection.setRequestMethod("GET");
				break;

			case POST:
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-type", "application/json; charset=" + "UTF-8");
		    	connection.setDoOutput(true);
		    	connection.setDoInput(true);
				break;
				
			case MULTIPART:

				connection.setRequestMethod("POST");
				connection.setUseCaches(false);
				connection.setDoOutput(true); // indicates POST requestMethod
				connection.setDoInput(true);
				connection.setRequestProperty("Content-Type","multipart/form-data; boundary=" + MultipartUtility.boundary);
				break;

		
		}
        
    	return connection;

    }

	/**
	 * This method binds the requested headers with the connection.
	 *
	 * @param connection HttpURLConnection instance with which headers needs to be add.
	 *
	 * @param headers Map containing the header information.
	 *
	 */
 
    private void addHeaders(HttpURLConnection connection, Map<String, String> headers){
    	if(headers!=null){
    		String keys[] = headers.keySet().toArray(new String[]{});
    		for (int i = 0; i < keys.length; i++) {
				connection.setRequestProperty(keys[i], headers.get(keys[i]));
			}
    	}
    }
    
    /**
	 * This method can be override by sub classes to perform any specific operation
	 * after conncetion establishment.
	 *
	 * @param conn HttpURLConnection instance having connection information.
	 *
	 * @return true if connection is fine and operation can be proceed further.
	 */

    protected boolean onConnection(HttpURLConnection conn){
        return true;
    }

	/**
	 * This method converts the InputStream into String.
	 *
	 * @param is InputStream which needs to be converted.
	 *
	 * @return Return the converted String if operation performed successfully.
	 *
	 * @throws OutOfMemoryError, IOException If any error occurred during process.
	 *
	 */
    protected String getString(InputStream is) throws OutOfMemoryError, IOException{
        
        StringWriter writer = new StringWriter();
        IOUtils.copy(is, writer, "UTF-8");
        return (interrupted)?null:writer.toString();
        
    }
   
    /** 
     * This method is invoked in sub class when stream is received from server.
	 *
     * @param is InputStream which contains the data.
     * 
     * @return true if stream is processed by subclass otherwise return false.
	 *
	 * @throws JResponseError It might throw the exception.
	 *
     */
    
    protected abstract boolean onStreamReceived(final InputStream is) throws JResponseError;

	/**
	 * This method is invoked in sub class when error stream is received from server.
	 * @param is InputStream which contains the data.
	 *
	 */

	protected abstract void onErrorStream(final InputStream is) throws JResponseError;
    
    /** 
     * This requestMethod is invoked when response processed from input stream.
	 *
     * @param response The string formatted response.
     *
     * @throws JResponseError It might throw the exception.
	 *
     */
    protected abstract void onResponseReceived(final String response) throws JResponseError;
    
    /** 
     * This method is invoked in sub class when error occurred during api request/response.
	 *
	 * @param e Exception having the error information.
	 *
     */

    protected abstract void onError(final Exception e);


   
}