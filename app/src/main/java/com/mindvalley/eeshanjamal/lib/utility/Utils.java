/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.lib.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.lang.reflect.Field;

public class Utils {

    public static final String TAG = "ash_utils";

    public static boolean isNetworkConnected(Context ctx) {

        boolean status = false;
        try{
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = cm.getActiveNetworkInfo();
            if (ni != null && ni.isConnectedOrConnecting()){
                status = true;
            }
            else{
                status = false;
            }
        }
        catch(NullPointerException e){
            e.printStackTrace();
            Log.e(TAG, e.getMessage()+"At isNetworkConnected(...) of Utils");
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage()+"At isNetworkConnected(...) of Utils");
        }

        return status;

    }

}
