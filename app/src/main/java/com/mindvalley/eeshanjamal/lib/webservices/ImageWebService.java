/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.lib.webservices;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.mindvalley.eeshanjamal.lib.cache.ImageCache;
import com.mindvalley.eeshanjamal.lib.webservices.support.JResponseError;

import java.io.InputStream;

public class ImageWebService extends WebService implements Runnable{

    public interface ImageWebServiceCallback{
        void onSuccess(String url, Bitmap bitmap);
        void onFailure(Exception e);
    }

    private ImageCache cache;
    private ImageWebServiceCallback callback;

    /**
     * This constructor is define for making OPEN server calls.
     *
     * @param ctx The current context making this request.
     *
     * @param url Url having the server data path.
     *
     * @param cache Memory Cache which is going to be used for storing the response information.
     *
     * @param callback Callback instance notify about the request success/failure.
     *
     */

    public ImageWebService(Context ctx, String url, ImageCache cache, ImageWebServiceCallback callback) {
        super(ctx);
        this.cache      = cache;
        this.callback   = callback;

        setupReqParams(ServerReuqestMethod.OPEN, url, null, null, null);

    }

    @Override
    public void run() {

        Bitmap bitmap = null;

        //Try fetching it through cache.
        if (cache!=null){
            bitmap = cache.get(requestUrl);
        }

        if (bitmap == null){
            connect();
        }
        else if(callback!=null){
            callback.onSuccess(requestUrl, bitmap);
        }

    }

    @Override
    protected boolean onStreamReceived(InputStream is) throws JResponseError {

        Bitmap bitmap = BitmapFactory.decodeStream(is);

        if (cache!=null)
            cache.put(requestUrl, bitmap);

        if (callback!=null)
            callback.onSuccess(requestUrl, bitmap);

        return true;

    }

    @Override
    protected void onErrorStream(InputStream is) throws JResponseError {
        if (callback!=null)
            callback.onFailure(new JResponseError("Error in Input Stream!!"));
    }

    @Override
    protected void onResponseReceived(String response) throws JResponseError {
        //Nothing need to be done here as the response is already handled through InputStream callback.
    }

    @Override
    protected void onError(Exception e) {
        if (callback!=null)
            callback.onFailure(e);
    }

}
