/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.eeshanjamal.lib.webservices.support;

import android.content.Context;
import android.util.Log;

import com.mindvalley.eeshanjamal.R;

import java.util.HashMap;

public class JResponseError extends Exception{

	public static final String TAG 			= "ash_jrerror";
	public static final int DEFAULT_CODE 	= -1;
	public static final String DEFAULT_TYPE = "ErrorType Not Found!!!";


	private String code = String.valueOf(DEFAULT_CODE);
	private String type = DEFAULT_TYPE;
	
	public static final String MSG  	 		= "message";
	public static final String ERROR  	 		= "error";
	public static final String CODE 	 		= "code";
	public static final String TYPE 	 		= "type";
	
	public static JResponseError getNetConnectionError(Context context){
		return new JResponseError(context.getString(R.string.no_internet));
	}
	
	//===
	
	public JResponseError(HashMap<String, String> info){
		super((info!=null)?info.get(MSG):"Null");
		
		if(info!=null){
			
			String code = info.get(CODE);
			if(code!=null && code.length()>0){
				this.code = code;
			}
			
			String type = info.get(TYPE);
			if(type!=null && type.length()>0){
				this.type =type;  
			}
			
		}
		
	}
	
	public JResponseError(String msg, String code, String type){ 
		super(msg);
		this.code = code;
		this.type = type;
	}
	
	//===
	public JResponseError(String msg){
		super(msg);
	}
	
	public JResponseError(){ 
		super();
	}
	
	public int getCode() {
		
		try{
			return Integer.parseInt(code);
		}
		catch(NumberFormatException e){
			e.printStackTrace();
			Log.e(TAG, e.getMessage()+" At getCode() of JResponseError");
		}
		
		return DEFAULT_CODE;
		
	}
	
	public String getCodeStr() {
		return code;
	}

	public String getType() {
		return type;
	}
	
	@Override
	public String getMessage() {
		Log.e(TAG, "ERROR_MSG : "+super.getMessage()+"\nERROR_CODE : "+code+"\nERROR_TYPE : "+type);
		return getMustMessage(super.getMessage());

	}

	private String getMustMessage(String msg){

		if (msg == null){
			msg = "Unknown Error";
		}
		return msg;
	}
	
}
