/*
 * Copyright (C) 2016 Eeshan Jamal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.mindvalley.eeshanjamal.lib.webservices;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mindvalley.eeshanjamal.R;
import com.mindvalley.eeshanjamal.lib.cache.JSONCache;
import com.mindvalley.eeshanjamal.lib.webservices.support.JResponseError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

public class JSONWebService extends WebService implements Runnable{

    public interface JSONObjectWebServiceCallback {
        void onSuccess(JsonElement response);
        void onFailure(Exception error);
    }

    public void setCallback(JSONObjectWebServiceCallback callback) {
        this.callback = callback;
    }

    private JSONObjectWebServiceCallback callback;
    private JSONCache cache;

    /**
     * This constructor is define for making GET API calls.
     *
     * @param ctx The current context making this request.
     *
     * @param url Url having the server api path.
     *
     * @param headers Headers information which is going to be bind with the connection.
     *
     * @param cache Memory Cache which is going to be used for storing the response information.
     *
     * @param callback Callback instance notify about the request success/failure.
     *
     */

    public JSONWebService(Context ctx, String url, Map<String, String> headers, JSONCache cache, JSONObjectWebServiceCallback callback) {
        super(ctx);
        setupReqParams(ServerReuqestMethod.GET, url, headers, null, null);
        initialize(cache, callback);
    }

    /**
     * This constructor is define for making POST API calls.
     *
     * @param ctx The current context making this request.
     *
     * @param url Url having the server api path.
     *
     * @param headers Headers information which is going to be bind with the connection.
     *
     * @param body Information which is going to be bind with the connection.
     *
     * @param cache Memory Cache which is going to be used for storing the response information.
     *
     * @param callback Callback instance notify about the request success/failure.
     *
     */

    public JSONWebService(Context ctx, String url, Map<String, String> headers, String body, JSONCache cache, JSONObjectWebServiceCallback callback) {
        super(ctx);
        setupReqParams(ServerReuqestMethod.POST, url, headers, body, null);
        initialize(cache, callback);
    }

    /**
     * This constructor is define for making MULTIPART API calls.
     *
     * @param ctx The current context making this request.
     *
     * @param url Url having the server api path.
     *
     * @param headers Headers information which is going to be bind with the connection.
     *
     * @param requestParam Information which is going to be bind with MULTI-PART connection.
     *
     * @param cache Memory Cache which is going to be used for storing the response information.
     *
     * @param callback Callback instance notify about the request success/failure.
     *
     */

    public JSONWebService(Context ctx, String url, Map<String, String> headers, Map<String, Object> requestParam, JSONCache cache, JSONObjectWebServiceCallback callback) {
        super(ctx);
        setupReqParams(ServerReuqestMethod.MULTIPART, url, headers, null, requestParam);
        initialize(cache, callback);
    }

    private void initialize(JSONCache cache, JSONObjectWebServiceCallback callback){
        this.cache      = cache;
        this.callback   = callback;
    }


    /**
     * This method is useful in finding error information insise error stream incase
     * any error occurred during connection establishment.
     *
     * @param stream ErrorStream containing error information.
     *
     * @return JResponseError instance having error information if found otherwise default error information.
     *
     */

    private JResponseError getStreamJResponseError(InputStream stream){

        String msg 			= ctx.getResources().getString(R.string.default_response_error_msg);
        String code			= String.valueOf(JResponseError.DEFAULT_CODE);
        String type			= JResponseError.DEFAULT_TYPE;
        JSONObject jsonObj	= null;

        try {
            String responseStr = getString(stream);
            jsonObj = (responseStr.length()>0)?new JSONObject(responseStr):new JSONObject();
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage()+" At getStreamJResponseError(...) of WebService");
        }
        catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage()+" At getStreamJResponseError(...) of WebService");
        }
        catch (OutOfMemoryError e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage()+" At getStreamJResponseError(...) of WebService");
        }


        if(jsonObj == null){
            return new JResponseError(msg, code, type);
        }
        else{
            return getJResponseError(jsonObj, msg, code, type);
        }

    }

    private JResponseError getJResponseError(JSONObject jsonObj, String msg, String code, String type){

        msg		= jsonObj.optString(JResponseError.MSG, jsonObj.optString(JResponseError.ERROR, msg));
        code	= jsonObj.optString(JResponseError.CODE, code);
        type	= jsonObj.optString(JResponseError.TYPE, type);

        return new JResponseError(msg, code, type);

    }

    @Override
    public void run() {

        JsonElement response = null;

        if (cache!=null)
            response = cache.get(requestUrl);

        if (response == null)
            connect();
        else if (callback!=null)
            callback.onSuccess(response);

    }

    @Override
    protected boolean onStreamReceived(InputStream is) throws JResponseError {

        JsonElement response = new Gson().fromJson(new InputStreamReader(is), JsonElement.class);

        if (cache!=null)
            cache.put(requestUrl, response);

        if (callback!=null)
            callback.onSuccess(response);

        return true;

    }

    @Override
    protected void onErrorStream(InputStream is) throws JResponseError {
        if (callback!=null)
            callback.onFailure(getStreamJResponseError(is));

    }

    @Override
    protected void onResponseReceived(String response) throws JResponseError {
        //Nothing need to be done here as the response is already handled through InputStream callback.
    }

    @Override
    protected void onError(Exception e) {
        if (callback!=null)
            callback.onFailure(e);
    }
}
